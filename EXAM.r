library(ggplot2)
library(dplyr)
library(caTools)
library(rpart.plot)
library(rpart)
library(pROC)
library(tm)
library(randomForest)
library(lubridate)

##Q1 -1

during <- (hms(as.character(train$CallEnd)) - hms(as.character(train$CallStart)))

##uplod the data set
setwd("C:/Users/Shahar/Desktop")
train.raw <- read.csv("train.csv")
train <- train.raw
str(train)
##Q1 - 2

train$Id  <- NULL
train$Default <- NULL
train$LastContactMonth <- NULL
train$PrevAttempts <- NULL
train$CallEnd <- NULL
train$CallStart  <- NULL
train$Marital    <- NULL
train$DaysPassed     <- NULL
train$HHInsurance     <- NULL
train$Outcome    <- NULL



##Q2 - 1

str(train)
##convert to factor

train$CarLoan <- as.factor(train$CarLoan )
train$CarInsurance <- as.factor(train$CarInsurance )

##Q2 - 2

##empty data


summary(train)

table(train$Education)



train[is.na(train$Education),'Education'] <- 'secondary'

train[is.na(train$Job),'Job'] <- 'management'

train[is.na(train$Communication),'Communication'] <- 'cellular'



##Q2  
##check about coarceing
str(train)
table(train$Education)
table(train$Education)
table(train$LastContactDay)
table(train$Age )

limit.NoOfContacts <- function(NoOfContacts){
  if (NoOfContacts > 25) return (25)
  return (NoOfContacts)
}

train$NoOfContacts <- sapply(train$NoOfContacts,limit.NoOfContacts)

table(train$Balance )

ggplot(train, aes(Balance)) + geom_histogram(binwidth = 3000)

limit.Balance <- function(Balance){
  if (Balance > 12000) return (12000)
  return (Balance)
}
train$Balance <- sapply(train$Balance,limit.Balance)

limit.age <- function(age){
  if (age > 79 ) return (79)
  return (age)
}


train$Age <- sapply(train$Age,limit.age)


##dividing the data
conv_01 <- function(x){
  x <- if(x==1) return(TRUE)
  return(FALSE)
}

train$CarInsurance <- sapply(train$CarInsurance , conv_01)



filter <- sample.split(train$CarInsurance , SplitRatio = 0.7)
train.train <- subset(train, filter == T)
train.test <- subset(train, filter == F)



##Q3 - 1
##create a decison tree




model.dt <- rpart(CarInsurance   ~ ., train.train)
rpart.plot(model.dt, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)

predictions <- predict(model.dt,train.test)

table(actual,predictions[,1]>0.6)

##Q3 - 2
##create random forest model
library(randomForest)
model.rf <- randomForest(CarInsurance ~ ., data = train.train, importance = TRUE)

summary(model.rf)

prediction.rf <- predict(model.rf,train.test)

actual <- train.test$CarInsurance

cf.rf <-table(actual,prediction.rf > 0.6)


##Q3 - 3
##calaculate recall and precision

Recall <-cf.rf[2,2]/(cf.rf[2,2]+cf.rf[1,2])
precicion <- cf.rf[2,2]/(cf.rf[2,2]+cf.rf[2,1])


##Q3 -4 
##ROC CHART

rocCurveDC <- roc(actual,predictions, direction = ">", levels = c("TRUE", "FALSE"))
rocCurveRF <- roc(actual,prediction.rf, direction = ">", levels = c("TRUE", "FALSE"))

#plot the chart 
plot(rocCurveDC, col = 'red',main = "ROC Chart")
par(new = TRUE) 
plot(rocCurveRF, col = 'blue',main = "ROC Chart")


auc(rocCurveDC)
auc(rocCurveRF)
##Q4 - 1
##currently method
sum <- length(train$CarInsurance)*200
rev <- length(train$CarInsurance)*200 - length(train$CarInsurance)*100 

##new nethod
revenue <- cf.rf[2,2]+cf.rf[1,2]*200
sumtotal <-  cf.rf[2,2]+cf.rf[1,2]*100

net_total_revenue <- revenue - sumtotal


rate <- (cf.rf[2,2]+cf.rf[1,2]) / (cf.rf[2,2]+cf.rf[1,2] + cf.rf[2,1] + cf.rf[1,1])


##Q4 - 4

